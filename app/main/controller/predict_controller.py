from flask import request
from flask_restx import Resource

from ..util.dto import PredictDto
from ..service.prediction_service import  get_prediction
from typing import Dict, Tuple

api = PredictDto.api


@api.route('/get-prediction')
class Predict(Resource):
    @api.doc('get a prediction')
    def get(self):
        """get prediction for user"""
        nameOfTheCharacter = request.args.get('name')
        prediction = get_prediction()
        return {nameOfTheCharacter: prediction}


