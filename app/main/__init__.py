from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

from .config import config_by_name
from flask.app import Flask

import tensorflow as tf
from keras.models import load_model
import os.path
from os import path

db = SQLAlchemy()
flask_bcrypt = Bcrypt()

basedir = path.abspath(os.path.dirname(__file__))

def create_app(config_name: str) -> Flask:
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    flask_bcrypt.init_app(app)

    return app
