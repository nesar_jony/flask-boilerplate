import numpy as np
import tensorflow as tf
from keras.models import load_model
class Graph:
   __instance = None
   @staticmethod 
   def getInstance():
      if Graph.__instance == None:
         Graph()
      return Graph.__instance
   def __init__(self):
      if Graph.__instance != None:
         raise Exception("This class is a singleton!")
      else:
         Graph.__instance = self